#include "ship.h"

//------------------------------------------------------------------------------
// Ship::Ship (constructor)
//------------------------------------------------------------------------------
Ship::Ship (SHIP_TYPES type) {
  m_type = type;
  m_size = getSize (type);
  m_name = getName (type);
}

//------------------------------------------------------------------------------
// Ship::hit
//------------------------------------------------------------------------------
void Ship::hit() {
  if (!isSunk()) {
    m_timesHit++;
  }
}

//------------------------------------------------------------------------------
// Ship::isHit
//------------------------------------------------------------------------------
bool Ship::isHit() {
  return m_timesHit > 0;
}

//------------------------------------------------------------------------------
// Ship::isIntact
//------------------------------------------------------------------------------
bool Ship::isIntact() {
  return m_timesHit == 0;
}

//------------------------------------------------------------------------------
// Ship::isSunk
//------------------------------------------------------------------------------
bool Ship::isSunk() {
  return m_timesHit == m_size;
}

//------------------------------------------------------------------------------
// Ship::getSize
//------------------------------------------------------------------------------
short Ship::getSize() {
  return m_size;
}

//------------------------------------------------------------------------------
// Ship::getSize
//------------------------------------------------------------------------------
short Ship::getSize (SHIP_TYPES type) {
  short size;
  
  switch (type) {
    case SHIP_TYPES::CARRIER:
      size = 5;
      break;
    case SHIP_TYPES::BATTLESHIP:
      size = 4;
      break;
    case SHIP_TYPES::CRUISER:
    case SHIP_TYPES::SUBMARINE:
      size = 3;
      break;
    case SHIP_TYPES::DESTROYER:
      size = 2;
  }
  return size;
}

//------------------------------------------------------------------------------
// Ship::getType
//------------------------------------------------------------------------------
Ship::SHIP_TYPES Ship::getType() {
  return m_type;
}

//------------------------------------------------------------------------------
// Ship::getTimesTouched
//------------------------------------------------------------------------------
short Ship::getTimesHit() {
  return m_timesHit;
}

//------------------------------------------------------------------------------
// Ship::getName
//------------------------------------------------------------------------------
string Ship::getName (SHIP_TYPES type) {
  string name;
  
  switch (type) {
    case SHIP_TYPES::CARRIER:
      name = "CARRIER";
      break;
    case SHIP_TYPES::BATTLESHIP:
      name = "BATTLESHIP";
      break;
    case SHIP_TYPES::CRUISER:
      name = "CRUISER";
      break;
    case SHIP_TYPES::SUBMARINE:
      name = "SUBMARINE";
      break;
    case SHIP_TYPES::DESTROYER:
      name = "DESTROYER";
  }
  return name;
}
