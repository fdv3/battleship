#ifndef GAME_H
#define GAME_H

#include "battlefield.h"

class Game {
  public:
    enum class STATES: short {
      NOT_STARTED             = 0,
      GAME_SETUP              = 1,
      WAIT_FOR_ACTIVE_SHOT    = 2,
      WAIT_FOR_PASSIVE_REPORT = 3,
      ANTICHEAT               = 4,
      FINISHED                = 5,
    };
    Game (pair<short, short> size, short nShips);
    void clearScreen();
    void printHeader();
    void setup();
    Battlefield* getPlayer (short player);
    void start();
    void turn();
    void printBattlefields();
    void pressEnterToContinue();
    void play();

  private:
    pair<short, short> m_size;
    short m_nShips;
    Battlefield* m_player1;
    Battlefield* m_player2;
    STATES m_status = STATES::NOT_STARTED;
    short m_turn;
};

#endif