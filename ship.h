#ifndef SHIP_H
#define SHIP_H

#include <string>

using namespace std;

class Ship {
  public:
    enum class SHIP_TYPES: short {
      CARRIER    = 0,
      BATTLESHIP = 1,
      CRUISER    = 2,
      SUBMARINE  = 3,
      DESTROYER  = 4
    };
    Ship (SHIP_TYPES type);
    void hit();
    bool isHit();
    bool isIntact();
    bool isSunk();
    short getSize();
    static short getSize (SHIP_TYPES type);
    SHIP_TYPES getType();
    short getTimesHit();
    static string getName (SHIP_TYPES type);

  private:
    short m_size;
    short m_timesHit = 0;
    SHIP_TYPES m_type;
    string m_name;
};

#endif