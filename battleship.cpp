#include <iostream>
#include "game.h"

const pair<short, short> kSIZE = make_pair (10, 10);
const short kNUM_SHIPS = 5;

using namespace std;

int main (int argc, char *argv[]) {
  Game* game = new Game (kSIZE, kNUM_SHIPS);

  game->setup();
  game->start();
  game->play();
  return 0;
}