#include <tuple>
#include <vector>
#include <string>
#include <gtest/gtest.h>
#include "../ship.h"

using namespace std;
using namespace ::testing;

class ShipTest: public TestWithParam<tuple<Ship::SHIP_TYPES, short, string>> {
  public:
    virtual void SetUp() { 
      ship = new Ship (get<0>(GetParam()));
    }
    
    virtual void TearDown() {
      delete ship;
      ship = NULL;
    }

  protected:
    Ship* ship;
};

//------------------------------------------------------------------------------
// Unit test for the constructor
//------------------------------------------------------------------------------
TEST_P (ShipTest, constructor) {
  EXPECT_EQ (get<1>(GetParam()), ship->getSize());
  EXPECT_EQ (get<0>(GetParam()), ship->getType());
  EXPECT_EQ (0, ship->getTimesHit());
  EXPECT_EQ (0, ship->isHit());
  EXPECT_EQ (1, ship->isIntact());
  EXPECT_EQ (0, ship->isSunk());
  EXPECT_EQ (get<1>(GetParam()), Ship::getSize (get<0>(GetParam())));
  EXPECT_EQ (get<2>(GetParam()), Ship::getName (get<0>(GetParam())));
}

//------------------------------------------------------------------------------
// Unit test for the hit method
//------------------------------------------------------------------------------
TEST_P (ShipTest, hit) {
  unsigned short i;

  ship->hit();
  EXPECT_EQ (1, ship->getTimesHit());
  EXPECT_EQ (1, ship->isHit());
  EXPECT_EQ (0, ship->isIntact());
  EXPECT_EQ (0, ship->isSunk());
  for (i = 2; i < get<1>(GetParam()); i++) {
    ship->hit();
    EXPECT_EQ (i, ship->getTimesHit());
    EXPECT_EQ (1, ship->isHit());
    EXPECT_EQ (0, ship->isIntact());
    EXPECT_EQ (0, ship->isSunk());
  }
  for (i = 0; i < 2; i++) {
    ship->hit();
    EXPECT_EQ (get<1>(GetParam()), ship->getTimesHit());
    EXPECT_EQ (1, ship->isHit());
    EXPECT_EQ (0, ship->isIntact());
    EXPECT_EQ (1, ship->isSunk());
  }
}

vector<tuple<Ship::SHIP_TYPES, short, string>> shipTypes = {
  make_tuple (Ship::SHIP_TYPES::CARRIER, 5, "CARRIER"),
  make_tuple (Ship::SHIP_TYPES::BATTLESHIP, 4, "BATTLESHIP"),
  make_tuple (Ship::SHIP_TYPES::CRUISER, 3, "CRUISER"),
  make_tuple (Ship::SHIP_TYPES::SUBMARINE, 3, "SUBMARINE"),
  make_tuple (Ship::SHIP_TYPES::DESTROYER, 2, "DESTROYER")
};

INSTANTIATE_TEST_CASE_P (ShipTestInstance, ShipTest, ValuesIn (shipTypes));
