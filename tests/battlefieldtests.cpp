#include <vector>
#include <gtest/gtest.h>
#include "../battlefield.h"

using namespace std;
using namespace ::testing;

const pair<short, short> kSIZE = make_pair (10, 10);
const short kNUM_SHIPS = 5;

class BattlefieldTest: public Test {
  public:
    virtual void SetUp() { 
      battlefield = new Battlefield (kSIZE, kNUM_SHIPS);
      battlefield->placeShipByPlayer (
        Ship::SHIP_TYPES::DESTROYER,
        make_pair (0, 0),
        Battlefield::ORIENTATION_TYPES::HORIZONTAL
      );
    }
    
    virtual void TearDown() {
      delete battlefield;
      battlefield = NULL;
    }

  protected:
    Battlefield* battlefield;
};

//------------------------------------------------------------------------------
// Unit test for the isValidCell method
//------------------------------------------------------------------------------
class IsValidCell: public BattlefieldTest, public WithParamInterface<pair<pair<short, short>, bool>> {};

TEST_P (IsValidCell, isValidCell) {
  EXPECT_EQ (GetParam().second, battlefield->isValidCell (GetParam().first));
}

vector<pair<pair<short, short>, bool>> isValidCellData = {
  make_pair (make_pair (-1, 0), 0),
  make_pair (make_pair (0, 0), 1),
  make_pair (make_pair (1, 0), 1),
  make_pair (make_pair (9, 0), 1),
  make_pair (make_pair (10, 0), 0),
  make_pair (make_pair (11, 0), 0),
  make_pair (make_pair (0, -1), 0),
  make_pair (make_pair (0, 0), 1),
  make_pair (make_pair (0, 1), 1),
  make_pair (make_pair (0, 9), 1),
  make_pair (make_pair (0, 10), 0),
  make_pair (make_pair (0, 11), 0),
  make_pair (make_pair (-1, 10), 0),
  make_pair (make_pair (6, 5), 1)
};

INSTANTIATE_TEST_CASE_P (BattlefieldTest, IsValidCell, ValuesIn (isValidCellData));

//------------------------------------------------------------------------------
// Unit test for the isFreeCell method
//------------------------------------------------------------------------------
class IsFreeCell: public BattlefieldTest, public WithParamInterface<pair<pair<short, short>, bool>> {};

TEST_P (IsFreeCell, isFreeCell) {
  EXPECT_EQ (GetParam().second, battlefield->isFreeCell (GetParam().first));
}

vector<pair<pair<short, short>, bool>> isFreeCellData = {
  make_pair (make_pair (-1, 0), 0),
  make_pair (make_pair (10, 0), 0),
  make_pair (make_pair (0, -1), 0),
  make_pair (make_pair (0, 10), 0),
  make_pair (make_pair (10, -1), 0),
  make_pair (make_pair (0, 0), 0),
  make_pair (make_pair (0, 1), 0),
  make_pair (make_pair (0, 2), 1),
  make_pair (make_pair (1, 0), 1),
};

INSTANTIATE_TEST_CASE_P (BattlefieldTest, IsFreeCell, ValuesIn (isFreeCellData));

//------------------------------------------------------------------------------
// Unit test for the isShipPlaced method
//------------------------------------------------------------------------------
TEST_F (BattlefieldTest, isShipPlaced) {
  EXPECT_EQ (1, battlefield->isShipPlaced (Ship::SHIP_TYPES::DESTROYER));
  EXPECT_EQ (0, battlefield->isShipPlaced (Ship::SHIP_TYPES::CARRIER));
  EXPECT_EQ (0, battlefield->isShipPlaced (Ship::SHIP_TYPES::CRUISER));
  EXPECT_EQ (0, battlefield->isShipPlaced (Ship::SHIP_TYPES::BATTLESHIP));
  EXPECT_EQ (0, battlefield->isShipPlaced (Ship::SHIP_TYPES::SUBMARINE));
}

//------------------------------------------------------------------------------
// Unit tests for the placeShip method
//------------------------------------------------------------------------------
TEST_F (BattlefieldTest, placeShipPlacedShipNonPlaceablePath) {
  vector<pair<short, short>> path;

  path = battlefield->placeShip (
    Ship::SHIP_TYPES::DESTROYER,
    make_pair (1, 2),
    Battlefield::ORIENTATION_TYPES::VERTICAL
  );
  EXPECT_EQ (0, path.size());
  EXPECT_EQ (1, battlefield->isShipPlaced (Ship::SHIP_TYPES::DESTROYER));
}

TEST_F (BattlefieldTest, placeShipPlacedShipPlaceablePath) {
  vector<pair<short, short>> path;

  path = battlefield->placeShip (
    Ship::SHIP_TYPES::DESTROYER,
    make_pair (2, 2),
    Battlefield::ORIENTATION_TYPES::VERTICAL
  );
  EXPECT_EQ (0, path.size());
  EXPECT_EQ (1, battlefield->isShipPlaced (Ship::SHIP_TYPES::DESTROYER));
}

TEST_F (BattlefieldTest, placeShipNonPlacedShipNonPlaceablePath) {
  vector<pair<short, short>> path;

  path = battlefield->placeShip (
    Ship::SHIP_TYPES::CARRIER,
    make_pair (0, 6),
    Battlefield::ORIENTATION_TYPES::HORIZONTAL
  );
  EXPECT_EQ (4, path.size());
  EXPECT_EQ (0, path[0].first);
  EXPECT_EQ (6, path[0].second);
  EXPECT_EQ (0, path[1].first);
  EXPECT_EQ (7, path[1].second);
  EXPECT_EQ (0, path[2].first);
  EXPECT_EQ (8, path[2].second);
  EXPECT_EQ (0, path[3].first);
  EXPECT_EQ (9, path[3].second);
  EXPECT_EQ (0, battlefield->isShipPlaced (Ship::SHIP_TYPES::CARRIER));
}

TEST_F (BattlefieldTest, placeShipNonPlacedShipPlaceablePath) {
  vector<pair<short, short>> path;

  path = battlefield->placeShip (
    Ship::SHIP_TYPES::BATTLESHIP,
    make_pair (0, 6),
    Battlefield::ORIENTATION_TYPES::VERTICAL
  );
  EXPECT_EQ (4, path.size());
  EXPECT_EQ (0, path[0].first);
  EXPECT_EQ (6, path[0].second);
  EXPECT_EQ (1, path[1].first);
  EXPECT_EQ (6, path[1].second);
  EXPECT_EQ (2, path[2].first);
  EXPECT_EQ (6, path[2].second);
  EXPECT_EQ (3, path[3].first);
  EXPECT_EQ (6, path[3].second);
  EXPECT_EQ (1, battlefield->isShipPlaced (Ship::SHIP_TYPES::BATTLESHIP));
}

TEST_F (BattlefieldTest, placeShipNonPlacedShipInvalidCell) {
  vector<pair<short, short>> path;

  path = battlefield->placeShip (
    Ship::SHIP_TYPES::CARRIER,
    make_pair (-1, -1),
    Battlefield::ORIENTATION_TYPES::HORIZONTAL
  );
  EXPECT_EQ (0, path.size());
  EXPECT_EQ (0, battlefield->isShipPlaced (Ship::SHIP_TYPES::CARRIER));
}

//------------------------------------------------------------------------------
// Unit tests for the placeShipByPlayer method
//------------------------------------------------------------------------------
TEST_F (BattlefieldTest, placeShipByPlayerPlacedShipNonPlaceablePath) {
  bool result;

  result = battlefield->placeShipByPlayer (
    Ship::SHIP_TYPES::DESTROYER,
    make_pair (1, 2),
    Battlefield::ORIENTATION_TYPES::VERTICAL
  );
  EXPECT_EQ (0, result);
  EXPECT_EQ (1, battlefield->isShipPlaced (Ship::SHIP_TYPES::DESTROYER));
}

TEST_F (BattlefieldTest, placeShipByPlayerPlacedShipPlaceablePath) {
  bool result;

  result = battlefield->placeShipByPlayer (
    Ship::SHIP_TYPES::DESTROYER,
    make_pair (2, 2),
    Battlefield::ORIENTATION_TYPES::VERTICAL
  );
  EXPECT_EQ (0, result);
  EXPECT_EQ (1, battlefield->isShipPlaced (Ship::SHIP_TYPES::DESTROYER));
}

TEST_F (BattlefieldTest, placeShipByPlayerNonPlacedShipNonPlaceablePath) {
  bool result;

  result = battlefield->placeShipByPlayer (
    Ship::SHIP_TYPES::CARRIER,
    make_pair (0, 6),
    Battlefield::ORIENTATION_TYPES::HORIZONTAL
  );
  EXPECT_EQ (0, result);
  EXPECT_EQ (0, battlefield->isShipPlaced (Ship::SHIP_TYPES::CARRIER));
}

TEST_F (BattlefieldTest, placeShipByPlayerNonPlacedShipPlaceablePath) {
  bool result;

  result = battlefield->placeShipByPlayer (
    Ship::SHIP_TYPES::BATTLESHIP,
    make_pair (0, 6),
    Battlefield::ORIENTATION_TYPES::VERTICAL
  );
  EXPECT_EQ (1, result);
  EXPECT_EQ (1, battlefield->isShipPlaced (Ship::SHIP_TYPES::BATTLESHIP));
}

TEST_F (BattlefieldTest, placeShipByPlayerNonPlacedShipInvalidCell) {
  bool result;

  result = battlefield->placeShipByPlayer (
    Ship::SHIP_TYPES::CARRIER,
    make_pair (-1, -1),
    Battlefield::ORIENTATION_TYPES::HORIZONTAL
  );
  EXPECT_EQ (0, result);
  EXPECT_EQ (0, battlefield->isShipPlaced (Ship::SHIP_TYPES::CARRIER));
}

//------------------------------------------------------------------------------
// Unit test for the string2cell method
//------------------------------------------------------------------------------
class Input2cell: public BattlefieldTest, public WithParamInterface<pair<string, pair<short, short>>> {};

TEST_P (Input2cell, input2cell) {
  pair<short, short> cell = battlefield->string2cell (GetParam().first);
  EXPECT_EQ (GetParam().second.first, cell.first);
  EXPECT_EQ (GetParam().second.second, cell.second);
}

vector<pair<string, pair<short, short>>> input2cellData = {
  make_pair ("", make_pair (-1, -1)),
  make_pair (" ", make_pair (-1, -1)),
  make_pair (" \t\n", make_pair (-1, -1)),
  make_pair ("aaa3333", make_pair (-1, -1)),
  make_pair ("123123asdasd", make_pair (-1, -1)),
  make_pair ("a-2", make_pair (-1, -1)),
  make_pair ("a - 2", make_pair (-1, -1)),
  make_pair ("2-a", make_pair (-1, -1)),
  make_pair ("-1a", make_pair (-1, -1)),
  make_pair ("<>ºª\\|!@\"#·$%&¬/()='¡?¿`+^*[]´ç¨Ç{},.-_:;", make_pair (-1, -1)),
  make_pair ("0a", make_pair (-1, -1)),
  make_pair ("F0", make_pair (-1, -1)),
  make_pair ("a11", make_pair (-1, -1)),
  make_pair ("4K", make_pair (-1, -1)),
  make_pair ("k11", make_pair (-1, -1)),
  make_pair ("A1", make_pair (0, 0)),
  make_pair ("10a", make_pair (0, 9)),
  make_pair ("j1", make_pair (9, 0)),
  make_pair ("10J", make_pair (9, 9)),
  make_pair ("e5", make_pair (4, 4)),
  make_pair ("8I", make_pair (8, 7))
};

INSTANTIATE_TEST_CASE_P (BattlefieldTest, Input2cell, ValuesIn (input2cellData));

//------------------------------------------------------------------------------
// Unit tests for the shotAndGetSunkShips methods
//------------------------------------------------------------------------------
TEST_F (BattlefieldTest, shotAndGetSunkShips) {
  pair <Battlefield::SHOT_RESULTS, Ship*> result;

  result = battlefield->shot ("K11");
  EXPECT_EQ (Battlefield::SHOT_RESULTS::ERROR, result.first);
  EXPECT_EQ ((Ship*) NULL, result.second);
  EXPECT_EQ (0, battlefield->getSunkShips());
  result = battlefield->shot ("b3");
  EXPECT_EQ (Battlefield::SHOT_RESULTS::MISS, result.first);
  EXPECT_EQ ((Ship*) NULL, result.second);
  EXPECT_EQ (0, battlefield->getSunkShips());
  result = battlefield->shot ("3B");
  EXPECT_EQ (Battlefield::SHOT_RESULTS::ALREADY_CALLED, result.first);
  EXPECT_EQ ((Ship*) NULL, result.second);
  EXPECT_EQ (0, battlefield->getSunkShips());
  result = battlefield->shot ("1A");
  EXPECT_EQ (Battlefield::SHOT_RESULTS::HIT, result.first);
  EXPECT_EQ (Ship::SHIP_TYPES::DESTROYER, result.second->getType());
  EXPECT_EQ (0, result.second->isSunk());
  EXPECT_EQ (1, result.second->isHit());
  EXPECT_EQ (0, result.second->isIntact());
  EXPECT_EQ (1, result.second->getTimesHit());
  EXPECT_EQ (0, battlefield->getSunkShips());
  result = battlefield->shot ("1A");
  EXPECT_EQ (Battlefield::SHOT_RESULTS::ALREADY_CALLED, result.first);
  EXPECT_EQ ((Ship*) NULL, result.second);
  EXPECT_EQ (0, battlefield->getSunkShips());
  result = battlefield->shot ("a2");
  EXPECT_EQ (Battlefield::SHOT_RESULTS::HIT, result.first);
  EXPECT_EQ (Ship::SHIP_TYPES::DESTROYER, result.second->getType());
  EXPECT_EQ (1, result.second->isSunk());
  EXPECT_EQ (1, result.second->isHit());
  EXPECT_EQ (0, result.second->isIntact());
  EXPECT_EQ (2, result.second->getTimesHit());
  EXPECT_EQ (1, battlefield->getSunkShips());
}