#include "ia.h"
#include "battlefield.h"

//------------------------------------------------------------------------------
// IA::IA (constructor)
//------------------------------------------------------------------------------
IA::IA (IA_TYPES type, Battlefield* opponentBattlefield) {
  m_type = type;
  m_name = getName (type);
  m_opponentBattlefield = opponentBattlefield;
}

//------------------------------------------------------------------------------
// IA::getName
//------------------------------------------------------------------------------
string IA::getName (IA_TYPES type) {
  string name;
  
  switch (type) {
    case IA_TYPES::EASY:
      name = "CPU EASY";
      break;
    case IA_TYPES::MEDIUM:
      name = "CPU MEDIUM";
      break;
    case IA_TYPES::HARD:
      name = "CPU HARD";
  }
  return name;
}

//------------------------------------------------------------------------------
// IA::getRandomNonShotCell
//------------------------------------------------------------------------------
pair<short, short> IA::getRandomNonShotCell() {
  pair<short, short> cell, size;

  size = m_opponentBattlefield->getSize();
  do {
    cell = make_pair (rand() % size.first, rand() % size.second);
  } while (find (m_shotCells.begin(), m_shotCells.end(), cell) != m_shotCells.end());
  return cell;
}
