CXX = g++
CXXFLAGS = -std=c++1z
LDFLAGS = -lgtest
COMMON_SOURCES = ship.cpp battlefield.cpp game.cpp ia.cpp
TARGET_SOURCES = battleship.cpp
COMMON_TEST_SOURCES = tests/shiptests.cpp tests/battlefieldtests.cpp
TEST_SOURCES = tests/testmain.cpp
EXECUTABLE = battleship
TEST_EXECUTABLE = tests/testrunner

.PHONY: all target tests clean cleantarget cleantests

all: clean target tests

target: cleantarget $(EXECUTABLE)

$(EXECUTABLE): $(COMMON_SOURCES) $(TARGET_SOURCES)
	$(CXX) $(CXXFLAGS) $^ -o $@

tests: cleantests $(TEST_EXECUTABLE)

$(TEST_EXECUTABLE): $(COMMON_SOURCES) $(COMMON_TEST_SOURCES) $(TEST_SOURCES)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $^ -o $@

cleantarget:
	rm -rf battleship

cleantests:
	rm -rf tests/testrunner

clean: cleantarget cleantests