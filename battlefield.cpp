#include <regex>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>
#include "ship.h"
#include "battlefield.h"

using namespace std;

//------------------------------------------------------------------------------
// Battlefield::Battlefield (constructor)
//------------------------------------------------------------------------------
Battlefield::Battlefield (pair<short, short> size, short nShips) {
  unsigned short i;
  vector<char> columns;

  m_size = size;
  m_nShips = nShips;
  for (i = 0; i < m_size.second; i++) {
    columns.push_back (WATER);
  }
  for (i = 0; i < m_size.first; i++) {
    m_secretMap.push_back (columns);
    m_playerMap.push_back (columns);
  }
}

//------------------------------------------------------------------------------
// Battlefield::printMap
//------------------------------------------------------------------------------
void Battlefield::printMap (MAP_TYPES type) {
  char headLetter;
  unsigned short i, j;

  cout << "   ";
  for (i = 0; i < m_size.second; i++) {
    cout << i + 1 << " ";
  }
  cout << endl;
  for (i = 0; i < m_size.first; i++) {
    headLetter = i + 65;
    cout << headLetter << "  ";
    for (j = 0; j < m_size.second; j++) {
      if (type == MAP_TYPES::PLAYER) {
        cout << m_playerMap[i][j];
      }
      else {
        cout << m_secretMap[i][j];
      }
      cout << " ";
    }
    cout << endl;
  }
}

//------------------------------------------------------------------------------
// Battlefield::placeShipsRandomly
//------------------------------------------------------------------------------
void Battlefield::placeShipsRandomly() {
  bool placed;
  unsigned short i, j;
  ORIENTATION_TYPES orientation;
  pair<short, short> start;
  short shipSize, placeablePathSize, end;
  vector<pair<short, short>> placeablePath;
  pair<pair<short, short>, ORIENTATION_TYPES> cell;
  vector<pair<pair<short, short>, ORIENTATION_TYPES>> visited;
  
  srand (time (NULL));
  for (i = 0; i < m_nShips; i++) {
    if (!isShipPlaced ((Ship::SHIP_TYPES) i)) {
      visited.clear();
      shipSize = Ship::getSize ((Ship::SHIP_TYPES) i);
      do {
        orientation = (ORIENTATION_TYPES) (rand() % 2);
        if (orientation == ORIENTATION_TYPES::HORIZONTAL) {
          end = m_size.second - shipSize;
          start = make_pair (rand() % m_size.first, rand() % (end + 1));
        }
        else if (orientation == ORIENTATION_TYPES::VERTICAL) {
          end = m_size.first - shipSize;
          start = make_pair (rand() % (end + 1), rand() % m_size.second);
        }
        cell = make_pair (start, orientation);
        if (find (visited.begin(), visited.end(), cell) == visited.end()) {
          visited.push_back (make_pair (start, orientation));
          placeablePath = placeShip ((Ship::SHIP_TYPES) i, start, orientation);
          placeablePathSize = placeablePath.size();
          if (placeablePathSize != shipSize) {
            for (j = 0; j < placeablePathSize; j++) {
              visited.push_back (make_pair (placeablePath[j], orientation));
            }
          }
        }
      } while (placeablePathSize != shipSize);
    }
  }
}

//------------------------------------------------------------------------------
// Battlefield::isValidCell
//------------------------------------------------------------------------------
bool Battlefield::isValidCell (pair<short, short> cell) {
  return cell.first >= 0 &&
         cell.first < m_size.first &&
         cell.second >= 0 &&
         cell.second < m_size.second;
}

//------------------------------------------------------------------------------
// Battlefield::isFreeCell
//------------------------------------------------------------------------------
bool Battlefield::isFreeCell (pair<short, short> cell) {
  return isValidCell (cell) && m_secretMap[cell.first][cell.second] == WATER;
}

//------------------------------------------------------------------------------
// Battlefield::isShipPlaced
//------------------------------------------------------------------------------
bool Battlefield::isShipPlaced (Ship::SHIP_TYPES shipType) {
  unsigned short i;
  bool isShipPlaced = 0;
  short ships = m_ships.size();

  for (i = 0; i < ships && !isShipPlaced; i++) {
    if (m_ships[i]->getType() == shipType) {
      isShipPlaced = 1;
    }
  }
  return isShipPlaced;
}

//------------------------------------------------------------------------------
// Battlefield::placeShip
//------------------------------------------------------------------------------
vector<pair<short, short>> Battlefield::placeShip (
  Ship::SHIP_TYPES shipType,
  pair<short, short> start,
  ORIENTATION_TYPES orientation
) {
  char id;
  Ship* ship;
  pair<short, short> cell;
  vector<pair<short, short>> placeablePath;
  short i, iterator, placeablePathSize, shipSize;
  bool isPlaceablePath = 1;

  if (!isShipPlaced (shipType)) {
    ship = new Ship (shipType);
    if (orientation == ORIENTATION_TYPES::HORIZONTAL) {
      iterator = start.second;
    }
    else if (orientation == ORIENTATION_TYPES::VERTICAL) {
      iterator = start.first;
    }
    shipSize = Ship::getSize (shipType);
    for (i = iterator; i < iterator + shipSize && isPlaceablePath; i++) {
      if (orientation == ORIENTATION_TYPES::HORIZONTAL) {
        cell = make_pair (start.first, i);
      }
      else if (orientation == ORIENTATION_TYPES::VERTICAL) {
        cell = make_pair (i, start.second);
      }
      if (!isFreeCell (cell)) {
        isPlaceablePath = 0;
      }
      else {
        placeablePath.push_back (cell);
      }
    }
    if (isPlaceablePath) {
      placeablePathSize = placeablePath.size();
      m_ships.push_back (ship);
      id = m_ships.size() + 47;
      for (i = 0; i < placeablePathSize; i++) {
        m_secretMap[placeablePath[i].first][placeablePath[i].second] = id;
      }
    }
  }
  return placeablePath;
}

//------------------------------------------------------------------------------
// Battlefield::placeShipByPlayer
//------------------------------------------------------------------------------
bool Battlefield::placeShipByPlayer (
  Ship::SHIP_TYPES shipType,
  pair<short, short> start,
  ORIENTATION_TYPES orientation
) {
  vector<pair<short, short>> placeablePath;
  short shipSize = Ship::getSize (shipType);

  placeablePath = placeShip (shipType, start, orientation);
  return placeablePath.size() == shipSize;
}

//------------------------------------------------------------------------------
// Battlefield::string2cell
//------------------------------------------------------------------------------
pair<short, short> Battlefield::string2cell (string input) {
  pair<short, short> cell = make_pair (-1, -1);
  regex re ("^((([A-Z])([0-9]+))|(([0-9]+)([A-Z])))$");
  smatch m;
  string inputX, inputY;
  short x, y;

  transform (input.begin(), input.end(), input.begin(), ::toupper);
  if (regex_match (input, m, re)) {
    inputX = (m.str (3) != "") ? m.str (3): m.str (7);
    inputY = (m.str (4) != "") ? m.str (4): m.str (6);
    x = inputX[0] - 65;
    istringstream (inputY) >> y;
    y--;
    if (x >= 0 and x < m_size.first and y >= 0 and y < m_size.second) {
      cell = make_pair (x, y);
    }
  }
  return cell;
}

//------------------------------------------------------------------------------
// Battlefield::shot
//------------------------------------------------------------------------------
pair<Battlefield::SHOT_RESULTS, Ship*> Battlefield::shot (pair<short, short> cell) {
  short shipIndex;
  Ship* ship = NULL;

  if (cell.first == -1 && cell.second == -1) {
    return make_pair (SHOT_RESULTS::ERROR, ship);
  }
  if (find (m_calledCells.begin(), m_calledCells.end(), cell) != m_calledCells.end()) {
    return make_pair (SHOT_RESULTS::ALREADY_CALLED, ship);
  }
  if (m_secretMap[cell.first][cell.second] == WATER) {
    m_calledCells.push_back (cell);
    m_secretMap[cell.first][cell.second] = MISS;
    m_playerMap[cell.first][cell.second] = MISS;
    return make_pair (SHOT_RESULTS::MISS, ship);
  }
  m_calledCells.push_back (cell);
  shipIndex = m_secretMap[cell.first][cell.second];
  shipIndex -= 48;
  m_secretMap[cell.first][cell.second] = HIT;
  m_playerMap[cell.first][cell.second] = HIT;
  ship = m_ships[shipIndex];
  ship->hit();
  if (ship->isSunk()) {
    m_sunkShips++;
  }
  return make_pair (SHOT_RESULTS::HIT, ship);
}

//------------------------------------------------------------------------------
// Battlefield::shot
//------------------------------------------------------------------------------
pair<Battlefield::SHOT_RESULTS, Ship*> Battlefield::shot (string input) {
  pair<short, short> cell = string2cell (input);

  return shot (cell);
}

//------------------------------------------------------------------------------
// Battlefield::getSunkShips
//------------------------------------------------------------------------------
short Battlefield::getSunkShips() {
  return m_sunkShips;
}

//------------------------------------------------------------------------------
// Battlefield::getRandomUncalledCell
//------------------------------------------------------------------------------
pair<short, short> Battlefield::getRandomUncalledCell() {
  pair<short, short> cell;

  do {
    cell = make_pair (rand() % m_size.first, rand() % m_size.second);
  } while (find (m_calledCells.begin(), m_calledCells.end(), cell) != m_calledCells.end());
  return cell;
}

//------------------------------------------------------------------------------
// Battlefield::cell2string
//------------------------------------------------------------------------------
string Battlefield::cell2string (pair<short, short> cell) {
  string output;

  if (!isValidCell (cell)) {
    return "";
  }
  output.push_back (cell.first + 65);
  output += to_string (cell.second + 1);
  return output;
}

//------------------------------------------------------------------------------
// Battlefield::areAllShipsSunk
//------------------------------------------------------------------------------
bool Battlefield::areAllShipsSunk() {
  return m_sunkShips == m_nShips;
}

//------------------------------------------------------------------------------
// Battlefield::getSize
//------------------------------------------------------------------------------
pair<short, short> Battlefield::getSize() {
  return m_size;
}