#include <cstdlib>
#include <iostream>
#include <algorithm>
#include "game.h"
#include "ship.h"
#include "battlefield.h"

using namespace std;

//------------------------------------------------------------------------------
// Game::Game (constructor)
//------------------------------------------------------------------------------
Game::Game (pair<short, short> size, short nShips) {
  srand (time (NULL));
  m_size = size;
  m_nShips = nShips;
  m_player1 = new Battlefield (m_size, m_nShips);
  m_player2 = new Battlefield (m_size, m_nShips);
  m_turn = rand() % 2 + 1;
}

//------------------------------------------------------------------------------
// Game::clearScreen
//------------------------------------------------------------------------------
void Game::clearScreen() {
  if (system ("CLS")) {
    system( "clear");
  }
}

//------------------------------------------------------------------------------
// Game::printHeader
//------------------------------------------------------------------------------
void Game::printHeader() {
  clearScreen();
  cout << "BATTLESHIP - CONSOLE VERSION by Fernando D. Vicente" << endl;
  cout << "---------------------------------------------------" << endl << endl;
}

//------------------------------------------------------------------------------
// Game::setup
//------------------------------------------------------------------------------
void Game::setup() {
  string setup, start, orientation, shipName;
  bool shipPlaced;
  short i, type, shipSize;
  pair<short, short> cell;
  
  if (m_status == STATES::NOT_STARTED) {
    do {
      printHeader();
      cout << "Select your ships setup (1. Randomly, 2. Manually): ";
      getline (cin, setup);
    } while (setup != "1" && setup != "2");
    if (setup == "1") {
      m_player1->placeShipsRandomly();
    }
    else {
      for (i = 0; i < m_nShips; i++) {
        shipPlaced = 0;
        shipName = Ship::getName ((Ship::SHIP_TYPES) i);
        shipSize = Ship::getSize ((Ship::SHIP_TYPES) i);
        do {
          printHeader();
          m_player1->printMap (Battlefield::MAP_TYPES::SECRET);
          cout << endl;
          cout << "Introduce the start coordinates for your ";
          cout << shipName << " (size " << shipSize << "): ";
          getline (cin, start);
          cout << "Introduce the orientation for your " << shipName;
          cout << " (size " << shipSize << ") (1. Horizontal, 2. Vertical): ";
          getline (cin, orientation);
          cell = m_player1->string2cell (start);
          if (orientation == "1" || orientation == "2") {
            type = orientation[0];
            type -= 49;
            shipPlaced = m_player1->placeShipByPlayer (
              (Ship::SHIP_TYPES) i,
              cell,
              (Battlefield::ORIENTATION_TYPES) type
            );
          }
          if (!shipPlaced) {
            cout << "ERROR: invalid coordinates. Try again." << endl;
          }
          else {
            cout << "SUCCESS: " << shipName << " (size " << shipSize;
            cout << ") placed properly" << endl << endl;
          }
          pressEnterToContinue();
        } while (!shipPlaced);
      }
    }
    m_player2->placeShipsRandomly();
    m_status = STATES::GAME_SETUP;
  }
}

//------------------------------------------------------------------------------
// Game::getPlayer
//------------------------------------------------------------------------------
Battlefield* Game::getPlayer (short player) {
  if (player < 1 || player > 2) {
    return (Battlefield*) NULL;
  }
  return ((player == 1) ? m_player1: m_player2);
}

//------------------------------------------------------------------------------
// Game::start
//------------------------------------------------------------------------------
void Game::start() {
  if (m_status == STATES::GAME_SETUP) {
    printHeader();
    printBattlefields();
    m_status = STATES::WAIT_FOR_ACTIVE_SHOT;
  }
}

//------------------------------------------------------------------------------
// Game::turn
//------------------------------------------------------------------------------
void Game::turn() {
  string action, player, sunk = "";
  pair<Battlefield::SHOT_RESULTS, Ship*> shot;
  pair<short, short> cell;

  if (m_status == STATES::WAIT_FOR_ACTIVE_SHOT) {
    if (m_turn == 1) {
      player = "Player";
      do {
        printHeader();
        printBattlefields();
        cout << player << "'s turn. Introduce a coordinate: ";
        getline (cin, action);
        shot = m_player2->shot (action);
        if (shot.first == Battlefield::SHOT_RESULTS::ERROR) {
          cout << "ERROR: invalid coordinates. Try again." << endl;
          pressEnterToContinue();
        }
        else if (shot.first == Battlefield::SHOT_RESULTS::ALREADY_CALLED) {
          cout << "ERROR: already attacked coordinates. Try again." << endl;
          pressEnterToContinue();
        }
      } while (shot.first != Battlefield::SHOT_RESULTS::HIT && shot.first != Battlefield::SHOT_RESULTS::MISS);
      transform (action.begin(), action.end(), action.begin(), ::toupper);
    }
    else if (m_turn == 2) {
      player = "IA";
      cout << player << "'s turn" << endl;
      cell = m_player1->getRandomUncalledCell();
      action = m_player2->cell2string (cell);
      shot = m_player1->shot (cell);
      pressEnterToContinue();
    }
    printHeader();
    printBattlefields();
    if (shot.first == Battlefield::SHOT_RESULTS::MISS) {
      cout << action << " -> MISS." << endl;
    }
    else if (shot.first == Battlefield::SHOT_RESULTS::HIT) {
      if (shot.second->isSunk()) {
        sunk = " and SUNK";
      }
      cout << action << " -> HIT" << sunk << ". " << Ship::getName (shot.second->getType()) << "." << endl;
    }
    cout << "End of " << player << "'s turn." << endl;
    pressEnterToContinue();
    m_turn = (m_turn == 1) ? 2: 1;
    if (m_player1->areAllShipsSunk() || m_player2->areAllShipsSunk()) {
      m_status = STATES::FINISHED;
    }
  }
}

//------------------------------------------------------------------------------
// Game::printBattlefields
//------------------------------------------------------------------------------
void Game::printBattlefields() {
  cout << "PLAYER'S MAP" << endl << "------------" << endl << endl;
  m_player1->printMap (Battlefield::MAP_TYPES::SECRET);
  cout << endl << "OPPONENT'S MAP" << endl << "--------------" << endl << endl;
  m_player2->printMap (Battlefield::MAP_TYPES::PLAYER);
  cout << endl;
}

//------------------------------------------------------------------------------
// Game::pressEnterToContinue
//------------------------------------------------------------------------------
void Game::pressEnterToContinue() {
  cout << "Press ENTER to continue ";
  cin.ignore (numeric_limits<streamsize>::max(), '\n');
}

//------------------------------------------------------------------------------
// Game::play
//------------------------------------------------------------------------------
void Game::play() {
  do {
    turn();
  } while (m_status != STATES::FINISHED);
  if (m_player1->areAllShipsSunk()) {
    cout << "End of game. IA wins!" << endl;
  }
  else if (m_player2->areAllShipsSunk()) {
    cout << "End of game. Player wins!" << endl;
  }
}
