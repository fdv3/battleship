#ifndef BATTLEFIELD_H
#define BATTLEFIELD_H

#include <vector>
#include "ship.h"

using namespace std;

class Battlefield {
  public:
    char const WATER = '~';
    char const MISS = ' ';
    char const SHIP = 'o';
    char const HIT = '*';
    enum class MAP_TYPES: short {
      SECRET = 0,
      PLAYER = 1
    };
    enum class SHOT_RESULTS: short {
      HIT            = 0,
      MISS           = 1,
      ERROR          = 2,
      ALREADY_CALLED = 3
    };
    enum class ORIENTATION_TYPES: short {
      HORIZONTAL = 0,
      VERTICAL   = 1
    };
    Battlefield (pair<short, short> size, short nShips);
    void printMap (MAP_TYPES type);
    void placeShipsRandomly();
    bool isValidCell (pair<short, short> cell);
    bool isFreeCell (pair<short, short> cell);
    bool isShipPlaced (Ship::SHIP_TYPES shipType);
    vector<pair<short, short>> placeShip (
      Ship::SHIP_TYPES shipType,
      pair<short, short> start,
      ORIENTATION_TYPES orientation
    );
    bool placeShipByPlayer (
      Ship::SHIP_TYPES shipType,
      pair<short, short> start,
      ORIENTATION_TYPES orientation
    );
    pair<short, short> string2cell (string input);
    pair<SHOT_RESULTS, Ship*> shot (string input);
    pair<SHOT_RESULTS, Ship*> shot (pair<short, short> cell);
    short getSunkShips();
    pair<short, short> getRandomUncalledCell();
    string cell2string (pair<short, short> cell);
    bool areAllShipsSunk();
    pair<short, short> getSize();

  private:
    pair<short, short> m_size;
    short m_nShips;
    vector<Ship*> m_ships;
    short m_sunkShips = 0;
    vector<vector<char>> m_secretMap;
    vector<vector<char>> m_playerMap;
    vector<pair<short, short>> m_calledCells;
};

#endif