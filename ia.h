#ifndef IA_H
#define IA_H

#include <string>
#include <vector>
#include "battlefield.h"

using namespace std;

class IA {
  public:
    enum class IA_TYPES: short {
      EASY   = 0,
      MEDIUM = 1,
      HARD   = 2
    };
    IA (IA_TYPES type, Battlefield* opponentBattlefield);
    static string getName (IA_TYPES type);
    pair<short, short> getRandomNonShotCell();

  private:
    string m_name;
    IA_TYPES m_type;
    Battlefield* m_opponentBattlefield;
    vector<pair<short, short>> m_shotCells;
};

#endif